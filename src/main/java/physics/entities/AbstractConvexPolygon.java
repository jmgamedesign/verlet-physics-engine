package physics.entities;

import physics.config.PhysicsConfig;
import physics.data.BoundingBox2D;
import physics.data.Projection2D;
import physics.data.Vector2D;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractConvexPolygon
{
    public final double mass;
    public final PhysicsConfig config;

    protected final List<AbstractPointMass> points = new ArrayList<>();
    protected final List<AbstractLineSegment> lines = new ArrayList<>();

    private volatile Vector2D center;

    private volatile BoundingBox2D boundingBox;

    protected AbstractConvexPolygon(double _mass, PhysicsConfig _config)
    {
        mass = _mass;
        config = _config;
    }

    //region getters and other such things
    public List<AbstractPointMass> getPoints()
    {
        return new ArrayList<>(points);
    }

    public boolean hasPoint(AbstractPointMass _p0)
    {
        return points.contains(_p0);
    }

    public List<AbstractLineSegment> getLines()
    {
        return new ArrayList<>(lines);
    }

    public boolean hasLine(AbstractLineSegment _l0)
    {
        return lines.contains(_l0);
    }

    public Vector2D getCenter()
    {
        return center;
    }

    public BoundingBox2D getBoundingBox()
    {
        return boundingBox;
    }
    //endregion

    //region physics
    public void updatePoints()
    {
        for (AbstractPointMass point : points)
        {
            point.update();
        }
    }
    //endregion

    //region relaxation
    public void relax()
    {
        updatePointsWorldConstraints();

        for (int i = 0; i < config.relaxationIterationsPerSimulationTick; i++)
        {
            updateLines();
        }

        calculateCenterAndBoundingBox();
    }

    private void updatePointsWorldConstraints()
    {
        for (AbstractPointMass point : points)
        {
            point.updateWorldConstraint();
        }
    }

    private void updateLines()
    {
        for (AbstractLineSegment line : lines)
        {
            line.update();
        }
    }

    public void calculateCenterAndBoundingBox()
    {
        int numPoints = points.size();

        Vector2D newCenter = new Vector2D();

        //We set these to the largest positive and negative values
        //so we can be sure our bounding box will be accurate when we start minmaxing.
        double minX = Double.MAX_VALUE;
        double minY = Double.MAX_VALUE;
        double maxX = -Double.MAX_VALUE;
        double maxY = -Double.MAX_VALUE;

        for (AbstractPointMass point : points)
        {
            if (!point.collidable)
            {
                continue;
            }

            //We add all of our vertices together...
            newCenter = newCenter.add(point.getPosition());

            //By performing these mins and maxes we can find the bounds of our bounding box.
            minX = Math.min(minX, point.getPosition().x);
            minY = Math.min(minY, point.getPosition().y);
            maxX = Math.max(maxX, point.getPosition().x);
            maxY = Math.max(maxY, point.getPosition().y);
        }

        //And then average their positions to get a center.
        newCenter = newCenter.div(numPoints);

        center = newCenter;
        boundingBox = new BoundingBox2D(minX, minY, maxX, maxY);
    }
    //endregion

    //region collision handling
    public static void detectCollision(AbstractConvexPolygon _c0, AbstractConvexPolygon _c1)
    {
        if (_c0 == _c1)
        {
            return;
        }

        if (_c0.getBoundingBox().intersects(_c1.getBoundingBox()))
        {
            detectComplexCollision(_c0, _c1);
        }
    }

    private static void detectComplexCollision(AbstractConvexPolygon _c0, AbstractConvexPolygon _c1)
    {
        List<AbstractLineSegment> c0Lines = _c0.lines;
        List<AbstractLineSegment> c1Lines = _c1.lines;

        int c0Size = c0Lines.size();
        int c0PlusC1Size = c0Lines.size() + c1Lines.size();

        //We'll need these later.
        double smallestDepth = Double.MAX_VALUE;
        Vector2D normal = new Vector2D();
        AbstractLineSegment c1l0 = null;
        AbstractPointMass c0p0 = null;

        boolean c1l0BelongsToC0 = false;

        //We'll iterate over both lists to find which line describes our best separating axis.
        for (int i = 0; i < c0PlusC1Size; i++)
        {
            AbstractLineSegment line;

            if (i < c0Size)
            {
                line = c0Lines.get(i);
            } else
            {
                line = c1Lines.get(i - c0Size);
            }

            //If a line isn't collidable we'll skip it.
            if (!line.collidable)
            {
                continue;
            }

            //Then we'll calculate the unit normal vector.
            Vector2D axis = line.getP1SubP0Vector();
            axis = axis.unitNormalVector();

            //Then we'll Project both bodies onto the unit normal vector.
            Projection2D c0Projection = _c0.ProjectToAxis(axis);
            Projection2D c1Projection = _c1.ProjectToAxis(axis);

            //Then we'll find the interval distance of the projections.
            double intervalDistance = Projection2D.projectionIntervalDistance(c0Projection, c1Projection);

            double absIntervalDistance = Math.abs(intervalDistance);

            //If the interval distance is greater than 0 there's no overlap
            //ergo no collision.
            if (intervalDistance > 0)
            {
                return;
            } else if (absIntervalDistance < smallestDepth) //We want to find where there's the smallest overlap so we get a realistic looking response.
            {
                //We're building our set of collision info here.
                smallestDepth = absIntervalDistance;
                normal = axis;

                c1l0BelongsToC0 = i < c0Size;
                c1l0 = line;
            }
        }

        //We want to make sure _c1 contains the collision edge to make things easier for us.
        if (c1l0BelongsToC0)
        {
            //So we'll swap _c0 and _c1 if they don't.
            AbstractConvexPolygon temp = _c1;
            _c1 = _c0;
            _c0 = temp;
        }


        //We take _c0's center into relative space relative to _c1's center.
        Vector2D c0CenterSubC1Center = _c0.getCenter().sub(_c1.getCenter());
        //Then we take the dot product of the relative center and the normal.
        double dotPDirection = normal.dot(c0CenterSubC1Center);

        //If the dotPDirection is negative it points away from C1, which we don't want.
        if (dotPDirection < 0)
        {
            normal.mul(-1);
        }


        //We, again, will set this to a large value because we'll be minning.
        double minDistance = Double.MAX_VALUE;

        List<AbstractPointMass> c0Points = _c0.points;

        for (AbstractPointMass point : c0Points)
        {
            //If the point isn't marked for collision checking.
            if (!point.collidable)
            {
                continue;
            }

            //We're just doing the same thing we did earlier, except now with all of _c0's points.
            //The reason we made sure the normal points toward C1 is so any points outside of the polygon
            //will wind up with a positive value.
            //We want to find which point is deepest into the polygon.
            //More than one point might intersect, but multiple iterations of this will take care of that.
            Vector2D pointSubC1Center = point.getPosition().sub(_c1.getCenter());
            double dotPDistance = normal.dot(pointSubC1Center);

            if (dotPDistance < minDistance)
            {
                minDistance = dotPDistance;
                c0p0 = point;
            }
        }

        //This is all of the information we need about the collision to calculate a response.
        handleCollision(smallestDepth, normal, c1l0, _c1.mass, _c1, c0p0, _c0.mass, _c0);
    }

    public Projection2D ProjectToAxis(Vector2D _axis)
    {
        double min = Double.MAX_VALUE;
        double max = -Double.MAX_VALUE;

        for (AbstractPointMass point : points)
        {
            if (!point.collidable)
            {
                continue;
            }

            double dotProduct = _axis.dot(point.getPosition());
            min = Math.min(dotProduct, min);
            max = Math.max(dotProduct, max);
        }

        return new Projection2D(min, max);
    }

    private static void handleCollision(double _depth, Vector2D _normal, AbstractLineSegment _c1l0, double _edgeMass, AbstractConvexPolygon _c1, AbstractPointMass _c0p0, double _pointMass, AbstractConvexPolygon _c0)
    {
        if (_normal == null || _c1l0 == null || _c0p0 == null)
        {
            return;
        }

        AbstractPointMass p0 = _c1l0.p0;
        AbstractPointMass p1 = _c1l0.p1;
        AbstractPointMass p2 = _c0p0;

        Vector2D collisionVector = _normal.mul(_depth);

        double t;

        if (Math.abs(p0.getPosition().x - p1.getPosition().x) > Math.abs(p0.getPosition().y - p1.getPosition().y))
        {
            t = (p2.getPosition().x - collisionVector.x - p0.getPosition().x) / (p1.getPosition().x - p0.getPosition().x);
        } else
        {
            t = (p2.getPosition().y - collisionVector.y - p0.getPosition().y) / (p1.getPosition().y - p0.getPosition().y);
        }

        double oneMinusT = 1.0 - t;

        double lambda = t * t + oneMinusT * oneMinusT;

        double CollisionMass = _edgeMass + _pointMass;

        double ratio1 = _pointMass / CollisionMass;
        double ratio2 = _edgeMass / CollisionMass;

        Vector2D p0DepthNormalRatio1 = collisionVector.mul(oneMinusT * ratio1 / lambda);
        p0.subPosition(p0DepthNormalRatio1);

        Vector2D p1DepthNormalRatio1 = collisionVector.mul(t * ratio1 / lambda);
        p1.subPosition(p1DepthNormalRatio1);

        Vector2D p2DepthNormalRatio2 = collisionVector.mul(ratio2);
        p2.addPosition(p2DepthNormalRatio2);

        double frictionCoeff = _c1l0.frictionCoeff + p2.frictionCoeff;

        if (frictionCoeff > 0)
        {
            Vector2D collisionVectorUnitNormal = collisionVector.unitNormalVector();

            p0.addFriction(collisionVectorUnitNormal, frictionCoeff * oneMinusT);
            p1.addFriction(collisionVectorUnitNormal, frictionCoeff * t);
            p2.addFriction(collisionVectorUnitNormal, frictionCoeff);
        }

        _c0.calculateCenterAndBoundingBox();
        _c1.calculateCenterAndBoundingBox();
    }
    //endregion
}
