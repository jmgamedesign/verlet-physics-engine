package physics.entities.standard;

import physics.config.PhysicsConfig;
import physics.data.Vector2D;
import physics.entities.AbstractPointMass;

public class StandardPointMass extends AbstractPointMass
{
    public StandardPointMass(double _x, double _y, double _frictionCoeff, boolean _collidable, PhysicsConfig _config)
    {
        super(_x, _y, _frictionCoeff, _collidable, _config);
    }

    public StandardPointMass(Vector2D _position, double _frictionCoeff, boolean _collidable, PhysicsConfig _config)
    {
        super(_position, _frictionCoeff, _collidable, _config);
    }
}
