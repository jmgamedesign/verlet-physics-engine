package physics.entities.standard;

import physics.config.PhysicsConfig;
import physics.entities.AbstractConvexPolygon;
import physics.entities.AbstractLineSegment;
import physics.entities.AbstractPointMass;

public class StandardSquarePolygon extends AbstractConvexPolygon
{
    public StandardSquarePolygon(double _x, double _y, double _width, double _height, double _stiffness, double _frictionCoeff, double _mass, PhysicsConfig _config)
    {
        super(_mass, _config);

        double frictionCoeff = _frictionCoeff/3;

        AbstractPointMass p0 = new StandardPointMass(_x, _y, frictionCoeff, true, config);
        AbstractPointMass p1 = new StandardPointMass(_x + _width, _y, frictionCoeff, true, config);
        AbstractPointMass p2 = new StandardPointMass(_x + _width, _y + _height, frictionCoeff, true, config);
        AbstractPointMass p3 = new StandardPointMass(_x, _y + _height, frictionCoeff, true, config);

        AbstractPointMass pm = new StandardPointMass(_x + _width / 2, _y + _height / 2, frictionCoeff, false, config);

        AbstractLineSegment l0 = new StandardLineSegment(p0, p1, _stiffness, true);
        AbstractLineSegment l1 = new StandardLineSegment(p1, p2, _stiffness, true);
        AbstractLineSegment l2 = new StandardLineSegment(p2, p3, _stiffness, true);
        AbstractLineSegment l3 = new StandardLineSegment(p3, p0, _stiffness, true);

        AbstractLineSegment l4 = new StandardLineSegment(p0, p2, _stiffness * 100, false);
        AbstractLineSegment l5 = new StandardLineSegment(p1, p3, _stiffness * 100, false);

        AbstractLineSegment l6 = new StandardLineSegment(p0, pm, _stiffness * 10, false);
        AbstractLineSegment l7 = new StandardLineSegment(p1, pm, _stiffness * 10, false);
        AbstractLineSegment l8 = new StandardLineSegment(p2, pm, _stiffness * 10, false);
        AbstractLineSegment l9 = new StandardLineSegment(p3, pm, _stiffness * 10, false);

        points.add(p0);
        points.add(p1);
        points.add(p2);
        points.add(p3);

        points.add(pm);


        lines.add(l0);
        lines.add(l1);
        lines.add(l2);
        lines.add(l3);

        lines.add(l4);
        lines.add(l5);

        lines.add(l6);
        lines.add(l7);
        lines.add(l8);
        lines.add(l9);
    }
}
