package physics.entities.standard;

import physics.entities.AbstractLineSegment;
import physics.entities.AbstractPointMass;

public class StandardLineSegment extends AbstractLineSegment
{
    public StandardLineSegment(AbstractPointMass _p0, AbstractPointMass _p1, double _stiffness, boolean _collidable)
    {
        super(_p0, _p1, _stiffness, _collidable);
    }
}
