package physics.entities;

import physics.config.PhysicsConfig;
import physics.data.Vector2D;

public abstract class AbstractPointMass
{
    protected volatile Vector2D position;
    protected volatile Vector2D oldPosition;

    public final double frictionCoeff;

    public final boolean collidable;

    public final PhysicsConfig config;

    protected volatile Vector2D velocity = Vector2D.ZERO_VECTOR;
    protected volatile Vector2D forces = Vector2D.ZERO_VECTOR;
    protected volatile Vector2D proportionalForces = Vector2D.ZERO_VECTOR;
    protected volatile Vector2D impulses = Vector2D.ZERO_VECTOR;

    private volatile boolean pinned;

    public AbstractPointMass(double _x, double _y, double _frictionCoeff, boolean _collidable, PhysicsConfig _config)
    {
        position = new Vector2D(_x, _y);
        oldPosition = position;

        frictionCoeff = _frictionCoeff;

        collidable = _collidable;

        config = _config;
    }

    public AbstractPointMass(Vector2D _position, double _frictionCoeff, boolean _collidable, PhysicsConfig _config)
    {
        position = _position;
        oldPosition = position;

        frictionCoeff = _frictionCoeff;

        collidable = _collidable;

        config = _config;
    }

    //region positioning
    public Vector2D getPosition()
    {
        return position;
    }

    public synchronized void setPosition(Vector2D _newPosition)
    {
        position = _newPosition;
    }

    public synchronized void addPosition(Vector2D _add)
    {
        position = position.add(_add);
    }

    public synchronized void subPosition(Vector2D _sub)
    {
        position = position.sub(_sub);
    }

    public Vector2D getOldPosition()
    {
        return oldPosition;
    }
    //endregion

    //region pinning
    public boolean isPinned()
    {
        return pinned;
    }

    public synchronized void setPin(boolean _pinned)
    {
        pinned = _pinned;
    }
    //endregion

    //region forces, impulses, proportional forces, and friction
    public synchronized void addForce(Vector2D _force)
    {
        forces = forces.add(_force);
    }

    public synchronized void addImpulse(Vector2D _impulse)
    {
        impulses = impulses.add(_impulse);
    }

    public synchronized void addProportionalForce(Vector2D _friction)
    {
        proportionalForces = proportionalForces.add(_friction);
    }

    public synchronized void addFriction(Vector2D _collisionVectorUnitNormal, double _frictionCoeff)
    {
        double velocityLen = velocity.len();

        double dotPDirection = _collisionVectorUnitNormal.dot(velocity);

        if (dotPDirection > 0)
        {
            _collisionVectorUnitNormal = _collisionVectorUnitNormal.mul(-1);
        }

        Vector2D friction = _collisionVectorUnitNormal.mul(velocityLen * _frictionCoeff);

        addProportionalForce(friction);
    }
    //endregion

    //region physics
    public synchronized void update()
    {
        //if the point isn't supposed to move, then we won't do anything
        if (pinned)
        {
            return;
        }

        //newPosition = 2 * position - oldPosition + acceleration * deltaTime * deltaTime
        //newPosition = position + position - oldPosition + acceleration * deltaTime * deltaTime
        //Therefore we can say:
        //velocity = position - oldPosition
        velocity = position.sub(oldPosition);

        //We're calculating drag as a minute proportional force in the opposite direction of velocity
        Vector2D drag = velocity.mul(-config.dragCoeff);
        addProportionalForce(drag);

        //Adding gravity.
        addForce(config.gravity);

        //Forces are changes in velocity over time, so they need to be in proportion to delta time squared.
        forces = forces.mul(config.deltaTimeSquared);
        //Proportional Forces are calculated relative to velocity, so they're implicitely also proportional to delta time squared in the general case.
        //proportionalForces = proportionalForces.mul(1);
        //Impulses are instantaneous changes in velocity, so it's sufficient to make them proportional to delta time.
        impulses = impulses.mul(config.deltaTime);

        velocity = velocity.add(forces).add(proportionalForces).add(impulses);

        //Our simulation has a max speed, so we need to make sure we don't exceed it.
        velocity = velocity.limit(config.maxSpeedTimesDeltaTime);

        Vector2D newPosition = position.add(velocity);

        //Setting up the verlet integration for next time.
        oldPosition = position;
        position = newPosition;
        forces = Vector2D.ZERO_VECTOR;
        impulses = Vector2D.ZERO_VECTOR;
        proportionalForces = Vector2D.ZERO_VECTOR;
    }

    public synchronized void updateWorldConstraint()
    {
        if (position.x < 0)
        {
            position = position.setX(0);
        }

        if (position.x > config.worldWidth)
        {
            position = position.setX(config.worldWidth);
        }

        if (position.y < 0)
        {
            position = position.setY(0);
        }

        if (position.y > config.worldHeight)
        {
            position = position.setY(config.worldHeight);
        }
    }
    //endregion
}
