package physics.entities;

import physics.data.Vector2D;

public abstract class AbstractLineSegment
{
    public final AbstractPointMass p0;
    public final AbstractPointMass p1;

    public final double idealLen;

    public final double stiffness;

    public final double frictionCoeff;

    public final boolean collidable;

    public AbstractLineSegment(AbstractPointMass _p0, AbstractPointMass _p1, double _stiffness, boolean _collidable)
    {
        p0 = _p0;
        p1 = _p1;

        idealLen = p0.getPosition().dst(p1.getPosition());

        if (_stiffness > 0.0 && _stiffness < 1.0)
        {
            stiffness = 1.0 - _stiffness;
        } else
        {
            stiffness = 0;
        }

        frictionCoeff = p0.frictionCoeff + p1.frictionCoeff;

        collidable = _collidable;
    }

    //region getters
    public Vector2D getP0Position()
    {
        return p0.getPosition();
    }

    public Vector2D getP1Position()
    {
        return p1.getPosition();
    }

    public Vector2D getP1SubP0Vector()
    {
        return p1.getPosition().sub(p0.getPosition());
    }
    //endregion

    //region relaxation
    public void update()
    {
        Vector2D p1SubP0Vector = getP1SubP0Vector().add(new Vector2D(.01f, .01f));

        double currentLen = p1SubP0Vector.len();

        double stiffnessToUse = 1.0 - stiffness;

        if (currentLen < idealLen)
        {
            stiffnessToUse = 1.0 - stiffness * currentLen / idealLen;
        }

        //This is equivalent to normalizing p1SubP0Vector,
        //multiplying it by the difference between where p0 and p1 are and where they should be
        //then dividing it by 2 so each p has equal responsibility to move.
        double differencePercent = stiffnessToUse * (currentLen - idealLen) / (currentLen * 2);

        p1SubP0Vector = p1SubP0Vector.mul(differencePercent);

        //If neither are pinned.
        if (!p0.isPinned() && !p1.isPinned())
        {
            p0.addPosition(p1SubP0Vector);
            p1.subPosition(p1SubP0Vector);
            return;
        }

        //If p1 is pinned.
        if (!p0.isPinned())
        {
            p0.addPosition(p1SubP0Vector);
            p0.addPosition(p1SubP0Vector);
            return;
        }

        //If p0 is pinned.
        if (!p1.isPinned())
        {
            p1.subPosition(p1SubP0Vector);
            p1.subPosition(p1SubP0Vector);
        }
    }
    //endregion
}
