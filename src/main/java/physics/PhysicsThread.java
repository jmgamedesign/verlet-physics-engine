package physics;

import physics.config.PhysicsConfig;

class PhysicsThread extends Thread
{
    private final PhysicsEngine engine;

    private final PhysicsConfig config;

    private volatile boolean running = true;

    public PhysicsThread(PhysicsEngine _engine, PhysicsConfig _config)
    {
        engine = _engine;

        config = _config;
    }

    public void stopPhysics()
    {
        running = false;
    }

    @Override
    public void run()
    {
        while (running)
        {
            long startTime = System.currentTimeMillis();

            for(int i = 0; i < config.uberTick; i++)
            {
                engine.tick();
            }

            long endTime = (System.currentTimeMillis() - startTime);

            long sleepTime = config.targetTimeInMSPerUberTick - endTime;

            double simulationSpeed = 100;

            if (sleepTime > 0)
            {
                try
                {
                    Thread.sleep(sleepTime);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            } else
            {
                double totalPercentTime = 1.0 - (double) sleepTime / config.targetTimeInMSPerUberTick;

                simulationSpeed /= totalPercentTime;
            }

            engine.postSimulationSpeed(simulationSpeed);
        }

    }
}
