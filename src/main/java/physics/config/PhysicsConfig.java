package physics.config;

import physics.data.Vector2D;

public class PhysicsConfig
{
    private static final double ONE_SECOND = 1.0;
    private static final double MS_PER_SECOND = 1000.0;

    public final int targetSimulationTicksPerSecond;
    public final int uberTick;
    public final int targetTimeInMSPerUberTick;

    public final double deltaTime;
    public final double deltaTimeSquared;

    public final int collisionIterationsPerSimulationTick;
    public final int relaxationIterationsPerSimulationTick;

    public final double maxSpeed;
    public final double maxSpeedTimesDeltaTime;

    public final Vector2D gravity;
    public final double dragCoeff;

    public final double worldWidth;
    public final double worldHeight;

    public final double horizontalWorldPartitionSize;
    public final int worldPartitionCount;

    public PhysicsConfig()
    {
        this(25,
                40,
                2,
                1,
                12000,
                new Vector2D(0, -98),
                .00005,
                1000,
                800,
                1000);
    }

    public PhysicsConfig(int _targetSimulationTicksPerSecond,
                         int _targetTimeInMSPerUberTick,
                         int _collisionIterationsPerSimulationTick,
                         int _relaxationIterationsPerSimulationTick,
                         double _maxSpeed, Vector2D _gravity,
                         double _dragCoeff,
                         double _worldWidth,
                         double _worldHeight,
                         double _horizontalWorldPartitionSize)
    {
        targetSimulationTicksPerSecond = _targetSimulationTicksPerSecond;
        targetTimeInMSPerUberTick = _targetTimeInMSPerUberTick;
        int tempUberTick = (int) Math.ceil(targetSimulationTicksPerSecond * targetTimeInMSPerUberTick / 1000.0);

        if (tempUberTick < 1)
        {
            uberTick = 1;
        } else
        {
            uberTick = tempUberTick;
        }

        deltaTime = ONE_SECOND / targetSimulationTicksPerSecond;
        deltaTimeSquared = deltaTime * deltaTime;

        collisionIterationsPerSimulationTick = _collisionIterationsPerSimulationTick;
        relaxationIterationsPerSimulationTick = _relaxationIterationsPerSimulationTick;

        maxSpeed = _maxSpeed;
        maxSpeedTimesDeltaTime = maxSpeed * deltaTime;

        gravity = _gravity;
        dragCoeff = _dragCoeff;

        worldWidth = _worldWidth;
        worldHeight = _worldHeight;

        if (_horizontalWorldPartitionSize > 0)
        {
            horizontalWorldPartitionSize = _horizontalWorldPartitionSize;
        } else
        {
            horizontalWorldPartitionSize = worldWidth;
        }

        worldPartitionCount = (int) (worldWidth / horizontalWorldPartitionSize) + 1;
    }
}
