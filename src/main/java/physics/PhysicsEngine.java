package physics;

import physics.config.PhysicsConfig;
import physics.data.BoundingBox2D;
import physics.data.WorldPartition;
import physics.entities.AbstractConvexPolygon;
import physics.entities.AbstractLineSegment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PhysicsEngine
{
    private final ExecutorService executor = Executors.newWorkStealingPool();

    private final PhysicsConfig config;
    private final WorldPartition[] worldPartitions;

    private final List<AbstractLineSegment> constraints = new ArrayList<>();
    private final List<AbstractConvexPolygon> polygons = new ArrayList<>();

    private volatile boolean started = false;

    public PhysicsThread physicsThread;

    private volatile double simulationSpeed = 100;

    public PhysicsEngine(PhysicsConfig _config)
    {
        config = _config;

        List<WorldPartition> worldPartitionsList = new ArrayList<>();

        double worldHeight = config.worldHeight;

        double horizontalWorldPartitionSize = config.horizontalWorldPartitionSize;
        int worldPartitionCount = config.worldPartitionCount;

        for (int i = -1; i < worldPartitionCount; i++)
        {
            BoundingBox2D boundingBox = new BoundingBox2D(i * horizontalWorldPartitionSize,
                    0,
                    (i + 1) * horizontalWorldPartitionSize,
                    worldHeight + 0);

            WorldPartition worldPartition = new WorldPartition(boundingBox);

            worldPartitionsList.add(worldPartition);
        }

        worldPartitions = new WorldPartition[worldPartitionsList.size()];

        worldPartitionsList.toArray(worldPartitions);
    }

    //region start physics
    public synchronized void startPhysics()
    {
        if (!started)
        {
            physicsThread = new PhysicsThread(this, config);
            executor.execute(physicsThread);
            started = true;
        }
    }
    //endregion stop physics

    //region physics entities
    public void addPolygon(AbstractConvexPolygon _c0)
    {
        if (polygons.contains(_c0))
        {
            return;
        }

        polygons.add(_c0);
    }

    public List<AbstractConvexPolygon> getPolygons()
    {
        return new ArrayList<>(polygons);
    }

    public void addConstraint(AbstractLineSegment _l0)
    {
        if (constraints.contains(_l0))
        {
            return;
        }

        constraints.add(_l0);
    }

    public List<AbstractLineSegment> getConstraints()
    {
        return new ArrayList<>(constraints);
    }
    //endregion

    //region simulation speed
    public void postSimulationSpeed(double _simulationSpeed)
    {
        simulationSpeed = _simulationSpeed;
    }

    public double getSimulationSpeed()
    {
        return simulationSpeed;
    }
    //endregion

    //region simulation
    void tick()
    {
        physics();

        response();
    }

    private void physics()
    {
        for (AbstractConvexPolygon polygon : polygons)
        {
            polygon.updatePoints();
        }
    }

    private void response()
    {
        for (AbstractLineSegment constraint : constraints)
        {
            for (int i = 0; i < config.relaxationIterationsPerSimulationTick; i++)
            {
                constraint.update();
            }
        }

        for (AbstractConvexPolygon polygon : polygons)
        {
            polygon.relax();
        }

        for (int i = 0; i < config.collisionIterationsPerSimulationTick; i++)
        {

            List<Callable<Object>> absorbTasks = new ArrayList<>();

            for (WorldPartition partition : worldPartitions)
            {
                absorbTasks.add(new Callable<Object>()
                {
                    @Override
                    public Object call()
                    {
                        for (AbstractConvexPolygon c0 : polygons)
                        {
                            partition.addPolygon(c0);
                        }

                        return null;
                    }
                });
            }

            try
            {
                executor.invokeAll(absorbTasks);
            }catch (Exception e)
            {
                return;
            }


            List<Callable<Object>> phase1Tasks = new ArrayList<>();

            for (int k = 0; k < worldPartitions.length; k += 2)
            {
                final int iterator = k;

                phase1Tasks.add(new Callable<Object>()
                {
                    @Override
                    public Object call()
                    {
                        WorldPartition left = worldPartitions[iterator];

                        List<AbstractConvexPolygon> leftPartitionPolygons = left.getPolygons();

                        WorldPartition right = null;

                        List<AbstractConvexPolygon> rightPartitionPolygons = null;

                        if ((iterator + 1) < worldPartitions.length)
                        {
                            right = worldPartitions[iterator + 1];

                            rightPartitionPolygons = right.getPolygons();
                        }

                        for (AbstractConvexPolygon c0 : leftPartitionPolygons)
                        {
                            for (AbstractConvexPolygon c1 : leftPartitionPolygons)
                            {
                                AbstractConvexPolygon.detectCollision(c0, c1);
                            }

                            if (rightPartitionPolygons == null)
                            {
                                continue;
                            }

                            for (AbstractConvexPolygon c1 : rightPartitionPolygons)
                            {
                                AbstractConvexPolygon.detectCollision(c0, c1);
                            }
                        }

                        return null;
                    }
                });
            }

            try
            {
                executor.invokeAll(phase1Tasks);
            }catch (Exception e)
            {
                return;
            }


            List<Callable<Object>> phase2Tasks = new ArrayList<>();

            for (int k = 1; k < worldPartitions.length; k += 2)
            {
                final int iterator = k;

                phase2Tasks.add(new Callable<Object>()
                {
                    @Override
                    public Object call()
                    {
                        WorldPartition left = worldPartitions[iterator];

                        List<AbstractConvexPolygon> leftPartitionPolygons = left.getPolygons();

                        WorldPartition right = null;

                        List<AbstractConvexPolygon> rightPartitionPolygons = null;

                        if ((iterator + 1) < worldPartitions.length)
                        {
                            right = worldPartitions[iterator + 1];

                            rightPartitionPolygons = right.getPolygons();
                        }

                        for (AbstractConvexPolygon c0 : leftPartitionPolygons)
                        {
                            for (AbstractConvexPolygon c1 : leftPartitionPolygons)
                            {
                                AbstractConvexPolygon.detectCollision(c0, c1);
                            }

                            if (rightPartitionPolygons == null)
                            {
                                continue;
                            }

                            for (AbstractConvexPolygon c1 : rightPartitionPolygons)
                            {
                                AbstractConvexPolygon.detectCollision(c0, c1);
                            }
                        }

                        return null;
                    }
                });
            }

            try
            {
                executor.invokeAll(phase2Tasks);
            }catch (Exception e)
            {
                return;
            }


            for (WorldPartition partition : worldPartitions)
            {
                partition.clearPolygons();
            }
            //*/

            /*
            for (AbstractConvexPolygon c0 : polygons)
            {
                for (AbstractConvexPolygon c1 : polygons)
                {
                    AbstractConvexPolygon.detectCollision(c0, c1);
                }
            }
            //*/
        }
    }

    private boolean countdownLatchWaiter(CountDownLatch _latch)
    {
        try
        {
            _latch.await();
        }catch (Exception e)
        {
            return true;
        }

        return false;
    }
    //endregion
}
