package physics.data;

public class BoundingBox2D
{
    public final double leftXMin;
    public final double bottomYMin;
    public final double rightXMax;
    public final double topYMax;

    public BoundingBox2D(double _leftXMin, double _bottomYMin, double _rightXMax, double _topYMax)
    {
        leftXMin = _leftXMin;
        bottomYMin = _bottomYMin;
        rightXMax = _rightXMax;
        topYMax = _topYMax;
    }

    public boolean intersects(BoundingBox2D _b)
    {
        return !(leftXMin > _b.rightXMax || bottomYMin > _b.topYMax || rightXMax < _b.leftXMin || topYMax < _b.bottomYMin);
    }
}
