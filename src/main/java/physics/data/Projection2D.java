package physics.data;

public class Projection2D
{
    public final double min;
    public final double max;

    public Projection2D(double _min, double _max)
    {
        min = _min;
        max = _max;
    }

    public static double projectionIntervalDistance(Projection2D c0Projection, Projection2D c1Projection)
    {
        if (c0Projection.min < c1Projection.min)
        {
            return c1Projection.min - c0Projection.max;
        } else
        {
            return c0Projection.min - c1Projection.max;
        }
    }
}
