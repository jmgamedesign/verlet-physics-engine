package physics.data;

public class Vector2D
{
    //Vector2D is immutable, which makes working with them less prone to side effects.

    //region statics
    public static final Vector2D X_VECTOR = new Vector2D(1, 0, 1, 1);
    public static final Vector2D Y_VECTOR = new Vector2D(0, 1, 1, 1);
    public static final Vector2D ZERO_VECTOR = new Vector2D();

    private static final double UNIT_VECTOR_LENGTH = 1.0;
    private static final double UNIT_VECTOR_LENGTH_SQUARED = UNIT_VECTOR_LENGTH;

    private static final double UNIT_VECTOR_ERROR = .02;
    private static final double UNIT_VECTOR_MIN_ERROR = 1.0 - UNIT_VECTOR_ERROR;
    private static final double UNIT_VECTOR_MAX_ERROR = 1.0 + UNIT_VECTOR_ERROR;

    private static final double NO_DIVISION_BY_ZERO_ALLOWED = .0001;
    //endregion

    //region member fields
    public final double x;
    public final double y;

    private double lenPrivate = -1;
    private double len2Private = -1;

    private Vector2D unitVectorPrivate = null;
    private Vector2D unitNormalVectorPrivate = null;

    private boolean isUnitVector;
    //endregion

    //region constructors
    public Vector2D()
    {
        x = 0.0;
        y = 0.0;
        lenPrivate = 0;
        len2Private = 0;
    }

    public Vector2D(double _x, double _y)
    {
        x = _x;
        y = _y;
    }

    private Vector2D(double _x, double _y, double _len)
    {
        x = _x;
        y = _y;
        lenPrivate = _len;
        len2Private = _len * _len;
    }

    private Vector2D(double _x, double _y, double _len, double _len2)
    {
        x = _x;
        y = _y;
        lenPrivate = _len;
        len2Private = _len2;
    }
    //endregion

    //region vector math

    public Vector2D setX(double _x)
    {
        return new Vector2D(_x, y);
    }

    public Vector2D setY(double _y)
    {
        return new Vector2D(x, _y);
    }

    public Vector2D add(Vector2D _v)
    {
        return new Vector2D(x + _v.x, y + _v.y);
    }

    public Vector2D sub(Vector2D _v)
    {
        return new Vector2D(x - _v.x, y - _v.y);
    }

    public Vector2D mul(double _scalar)
    {
        if (isUnitVector)
        {
            return new Vector2D(x * _scalar, y * _scalar, Math.abs(_scalar));
        }
        return new Vector2D(x * _scalar, y * _scalar);
    }

    public Vector2D mul(Vector2D _v)
    {
        return new Vector2D(x * _v.x, y * _v.y);
    }

    public Vector2D div(double _divisor)
    {
        if (_divisor == 0)
        {
            _divisor = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        if (isUnitVector)
        {
            return new Vector2D(x / _divisor, y / _divisor, Math.abs(UNIT_VECTOR_LENGTH / _divisor));
        }

        return new Vector2D(x / _divisor, y / _divisor);
    }

    public Vector2D div(Vector2D _v)
    {
        double vx = _v.x;
        double vy = _v.y;

        if (vx == 0)
        {
            vx = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        if (vy == 0)
        {
            vy = NO_DIVISION_BY_ZERO_ALLOWED;
        }

        return new Vector2D(x / vx, y / vy);
    }

    public double dot(Vector2D _v)
    {
        return x * _v.x + y * _v.y;
    }

    public double cross(Vector2D _v)
    {
        return x * _v.y - y * _v.x;
    }

    public double len()
    {
        if (lenPrivate == -1)
        {
            len2();

            if (!isUnitVector)
            {
                lenPrivate = Math.sqrt(len2Private);
            }
        }

        return lenPrivate;
    }

    public double len2()
    {
        if (len2Private == -1)
        {
            len2Private = dot(this);

            if (len2Private > UNIT_VECTOR_MIN_ERROR && len2Private < UNIT_VECTOR_MAX_ERROR)
            {
                lenPrivate = UNIT_VECTOR_LENGTH;
                len2Private = UNIT_VECTOR_LENGTH_SQUARED;
                unitVectorPrivate = this;
                isUnitVector = true;
            }
        }

        return len2Private;
    }

    public double dst(Vector2D _v)
    {
        return sub(_v).len();
    }

    public double dst2(Vector2D _v)
    {
        return sub(_v).len2();
    }

    public Vector2D unitVector()
    {
        if (unitVectorPrivate == null)
        {
            double len = len();

            if (len == 0)
            {
                unitVectorPrivate = ZERO_VECTOR;
                return unitVectorPrivate;
            }

            if (unitVectorPrivate != null)
            {
                return unitVectorPrivate;
            }

            //I did this instead of using the div method for speed and clarity reasons.
            unitVectorPrivate = new Vector2D(x / len, y / len, UNIT_VECTOR_LENGTH, UNIT_VECTOR_LENGTH_SQUARED);
        }

        return unitVectorPrivate;
    }

    public Vector2D unitNormalVector()
    {
        if (unitNormalVectorPrivate == null)
        {
            unitVector();

            unitNormalVectorPrivate = new Vector2D(unitVectorPrivate.y, -unitVectorPrivate.x, UNIT_VECTOR_LENGTH, UNIT_VECTOR_LENGTH_SQUARED);
        }

        return unitNormalVectorPrivate;
    }

    public Vector2D setLength2(double _len2)
    {
        return setLength(Math.sqrt(_len2));
    }

    public Vector2D setLength(double _len)
    {
        if(_len > UNIT_VECTOR_MIN_ERROR && _len < UNIT_VECTOR_MAX_ERROR)
        {
            return unitVector();
        }

        double oldLen = len();

        //Because if the length is zero there's no reasonable way to extend it in any direction. This just stops useless multiplication.
        if (oldLen == 0)
        {
            return ZERO_VECTOR;
        }

        return mul(_len / oldLen);
    }

    public Vector2D clamp2(double _min2, double _max2)
    {
        double oldLen2 = len2();

        if (oldLen2 < _min2)
        {
            return setLength2(_min2);
        }

        if (oldLen2 > _max2)
        {
            return setLength2(_max2);
        }

        return this;
    }

    public Vector2D clamp(double _min, double _max)
    {
        double oldLen2 = len2();

        if (oldLen2 < (_min * _min))
        {
            return setLength(_min);
        }

        if (oldLen2 > (_max * _max))
        {
            return setLength(_max);
        }

        return this;
    }

    public Vector2D limit2(double _len2)
    {
        return clamp2(0, _len2);
    }


    public Vector2D limit(double _len)
    {
        return clamp(0, _len);
    }

    //endregion

    @Override
    public String toString()
    {
        return "(" + x + " , " + y + ")";
    }
}
