package physics.data;

import physics.entities.AbstractConvexPolygon;

import java.util.ArrayList;
import java.util.List;

public class WorldPartition
{
    private final BoundingBox2D boundingBox;
    private final List<AbstractConvexPolygon> polygons = new ArrayList<>();

    public WorldPartition(BoundingBox2D _boundingBox)
    {
        boundingBox = _boundingBox;
    }

    public synchronized void addPolygon(AbstractConvexPolygon _c0)
    {
        if(_c0.getBoundingBox().intersects(boundingBox) && !polygons.contains(_c0))
        {
            polygons.add(_c0);
        }
    }

    public synchronized List<AbstractConvexPolygon> getPolygons()
    {
        return new ArrayList<>(polygons);
    }

    public synchronized void clearPolygons()
    {
        polygons.clear();
    }
}
